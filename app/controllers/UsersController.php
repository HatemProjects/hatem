<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


     	public function __construct()

    {
        //filter to normal users cant pass  to add or edit or delete
        $this->beforeFilter('isAdmin',array('only' => ['create','store','show','edit','update','destroy']));
    }

	public function index()
	{
		//
        //to show all users if user is admin or show user profile for normal user
        if(Auth::user()->user_type==2)  {
        $user=User::find(Auth::user()->id);
         return View::make('admin.users.user')
         ->with('user',$user);
        }
        elseif(Auth::user()->user_type==1)  {
        $users=User::all();
        return View::make('admin.users.users')
        ->with('users',$users);
         }
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        //to show add user form

       return View::make('admin.users.adduser');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
    // to store new user data
        $inputs=Input::all();

       $rules=array(
        'username'=>'required|unique:users,username|min:5',
        'fullname'=>'required',
        'email'=>'required',
        'password'=>'required|confirmed|min:5',
        'password_confirmation' => 'required|same:password'


       );

       $validator=Validator::make($inputs,$rules);

       if($validator->fails()) {

       return  Redirect::to('users/create')
       ->withInput()
       ->withErrors($validator);
       }else{


       $add = new User ;
       $add->username = Input::get('username');
       $add->fullname = Input::get('fullname');
       $add->email = Input::get('email');
       $add->user_type = 2;
       $add->password = Hash::make(Input::get('password'));



         if($add->save()){
				Session::flash('save','تم اضافه المستخدم بنجاح');
				return Redirect::to('users');
			}else{
				Session::flash('error','حدث خطأ , حاول مرة أخرى');
				return Redirect::to('users/create');
			}

    	}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//


	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//to show edit user form

         $useredit = User::find($id);
         return View::make('admin.users.edituser')
         ->with('useredit',$useredit);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
        //to update user data
         $inputs=Input::all();

       $rules=array(
        'username'=>'required|min:5|unique:users,username,'.$id,
        'fullname'=>'required',
        'email'=>'required',
        'password'=>'confirmed',
        'password_confirmation' => 'same:password'


       );

       $validator=Validator::make($inputs,$rules);

       if($validator->fails()) {

       return  Redirect::to('users/'.$id.'/edit')
       ->withInput()
       ->withErrors($validator);
       }else{


       $update 	         = User::find($id);
       $update->username = Input::get('username');
       $update->fullname = Input::get('fullname');
       $update->email    = Input::get('email');
       $update->password = Hash::make(Input::get('password'));


         if($update->save()){
				Session::flash('save','تم تعديل المستخدم بنجاح');
				return Redirect::to('users');
			}else{
				Session::flash('error','حدث خطأ , حاول مرة أخرى');
				return Redirect::to('users/'.$id.'/edit');
			}

    	}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//  to delete  user


        $delete = User::find($id);
		if($delete->delete()){
			Session::flash('save','تم حذف المستخدم بنجاح');
			return Redirect::to('/users');
		}else{
			Session::flash('error','حدث خطأ , حاول مرة أخرى');
			return Redirect::to('/users');
		}


	}


        public function showregister(){

        //to show register form
         if (Auth::check()) {
            return Redirect::to('/');
         }
     	return View::make('admin.layouts.register');

    }

     public function register(){
         //to register new user

     $inputs = Input::all();
		$rules = array(
	    'username'=>'required|min:5|unique:users,username',
        'fullname'=>'required',
        'email'=>'required',
        'password'=>'required|confirmed|min:5',
        'password_confirmation' => 'required|same:password'

					);

		$validator = Validator::make($inputs,$rules);
		if($validator->fails()){
			return Redirect::to('register')->withInput()->withErrors($validator);
		}else{



        $add = new User ;
       $add->username = Input::get('username');
       $add->fullname = Input::get('fullname');
       $add->email = Input::get('email');
       $add->user_type = 2;
       $add->password = Hash::make(Input::get('password'));



			if($add->save()){


				Session::flash('save','تم التسجيل بنجاح من فضلك سجل دخولك');
				return Redirect::to('login');
			}else{
				Session::flash('error','حدث خطأ , حاول مرة أخرى');
				return Redirect::to('register');
			}
		}//end of validation


     }


}
