<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//login
Route::get('login',function(){
    //check if user login and make him pass to users route
    if (Auth::check()) {
    return Redirect::to('/users');
 }else{
     	return View::make('admin.layouts.login');
 }
});

 //register

Route::get('register', 'UsersController@showregister');
Route::post('register', 'UsersController@register');









Route::post('login', array('before' => 'csrf',function(){
    //login
     	if(Auth::attempt(array('username'=>Input::get('username'),'password'=>Input::get('password'))))
	{
     	return Redirect::to('/');
    } else{
		Session::flash('loginerror', 'بيانات الدخول غير صحيحة');
		return View::make('admin.layouts.login');
	}
}));

//logout
Route::get('logout', function(){
 Auth::logout();

 return Redirect::to('login');
});


 //filter to check if user login or not before pass user controller or pass control panel
Route::group(array('before'=>'auth'),function(){
 //redirect any url with project name to users route
Route::get('/', function()
{
    return Redirect::to('/users');
});


Route::resource('users','UsersController');



});


