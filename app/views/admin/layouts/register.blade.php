<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
			@if(Session::get('language')=="ar")
			{{SettingsController::pharmysettings()->websitename_in_arabic}}
			  @elseif(Session::get('language')=="en")
			{{SettingsController::pharmysettings()->websitename_in_english}}
            @endif 
			| Registration Page
			</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('assets/plugins/iCheck/square/blue.css')}}">
	<link rel="stylesheet" href="{{asset('assets/front/css/font-awesome.min.css')}}">
	 <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/select2.min.css')}}">

    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition register-page">
  
		
		


	 @if($errors->any())
		<ul>
		{{implode('',$errors->all('
			<div class="container-fluid">
			<div class="alert alert-dismissable alert-danger  ">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
				<h4>:message</h4>					
				</div>
			</div>					
			'))}}			
		</ul>
		
		@endif
		
		
		<?php $error_check = Session::get('error');?>
		@if(!empty($error_check))
		<div class="container-fluid">
		<div class="alert alert-dismissable alert-danger">
		<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
			<h4>{{Session::get('error') }}</h4>
			{{Session::forget('error') }}
		</div>
		</div>
		@endif
		

		<?php $save_chack = Session::get('save');?>
		@if(!empty($save_chack))
		<div class="container-fluid">
		<div class="alert alert-dismissable alert-success">
		<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
		<h4>{{Session::get('save') }}</h4>
			{{Session::forget('save') }}
		</div>
		</div>
		@endif
				
  
    <div class="register-box   ">
	


      <div class="register-box-body col-lg-12 col-md-12  col-xs-12 col-sm-12">
        <p class="login-box-msg">Register</p>

	
				
	   {{Form::open(array(
		'url'=>'register',
		'class'=>'form-horizontal',
		'role'=>'form',
		'method' => 'POST'
		))}}	
        
          <div class="form-group has-feedback">
            <input type="text" name="fullname" class="form-control" value="{{Input::old('fullname')}}" placeholder="Full Name">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
		  
		  <div class="form-group has-feedback">
            <input type="text" name="username" class="form-control" value="{{Input::old('username')}}" placeholder="User Name">
            <span class="glyphicon glyphicon-pencil form-control-feedback"></span>
          </div>
		  
          <div class="form-group has-feedback">
            <input type="email" name="email" class="form-control" value="{{Input::old('email')}}" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
		
		
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password_confirmation" class="form-control" placeholder=" retype password">
            <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
          </div>
		  
		 
          <div class="row">
             
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div><!-- /.col -->
          </div>
       {{Form::close()}}

        
 
        
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->
 
    <!-- jQuery 2.1.4 -->
    <script src="{{asset('assets/plugins/jQuery/jQuery-2.1.4.min.js')}}"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('assets/plugins/iCheck/icheck.min.js')}}"></script>
	 <!-- Select2 -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
  
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
		//Initialize Select2 Elements

		  $(".select2").select2();

      });
    </script>
  </body>
</html>
