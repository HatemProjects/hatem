@extends('admin/layouts/main')

@section('content')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		
          <h1>
            Add User
           
          </h1>
          
		  
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Your Page Content Here -->
		   <div class="row">
		  
			
			   <!-- right column -->
            <div class="col-md-12">
			
              <!-- Horizontal Form -->
			  
              <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Horizontal Form</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
               
			   
		@if($errors->any())
		<ul>
		{{implode('',$errors->all('
			<div class="container-fluid">
			<div class="alert alert-dismissable alert-danger  ">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
				<h4>:message</h4>					
				</div>
			</div>					
			'))}}			
		</ul>
		
		@endif
		
		
		<?php $error_check = Session::get('error');?>
		@if(!empty($error_check))
		<div class="container-fluid">
		<div class="alert alert-dismissable alert-danger">
		<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
			<h4>{{Session::get('error') }}</h4>
			{{Session::forget('error') }}
		</div>
		</div>
		@endif
			   
			   
				
				
				{{Form::open(array(
				'url'=>'users',
				'class'=>'form-horizontal',
				'role'=>'form',
				'method' => 'POST'
				
				))}}
				
				
                  <div class="box-body">
				  
				  
				  
                    <div class="form-group">
                      <label  class="col-sm-2 control-label">Full Name</label>
                      <div class="col-sm-6">
                        <input type="text" name="fullname" value="{{Input::old('fullname')}}" class="form-control"  placeholder="Full Name">
                      </div>
                    </div>
					
					
					<div class="form-group">
                      <label  class="col-sm-2 control-label">User Name</label>
                      <div class="col-sm-6">
                        <input type="text" name="username" value="{{Input::old('username')}}" class="form-control"  placeholder="User Name">
                      </div>
                    </div>
					
					
					<div class="form-group">
                      <label  class="col-sm-2 control-label">Email</label>
                      <div class="col-sm-6">
                        <input type="email" name="email" value="{{Input::old('email')}}" class="form-control"  placeholder="Email">
                      </div>
                    </div>
					
					
					
					
                    <div class="form-group">
                      <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                      <div class="col-sm-6">
                        <input type="password" name="password" class="form-control"  placeholder="Password">
                      </div>
                    </div>
					
					<div class="form-group">
                      <label  class="col-sm-2 control-label">Confirm Password</label>
                      <div class="col-sm-6">
                        <input type="password" name="password_confirmation" class="form-control"  placeholder="Confirm Password">
                      </div>
                    </div>
					
					
					
                  </div><!-- /.box-body -->
                  <div class="box-footer">
                    
                    <button type="submit" class="btn btn-info pull-right">Save</button>
                  </div><!-- /.box-footer -->
				  
				  
               {{ Form::close() }}
				
				
              </div><!-- /.box -->
			  
			  
			 
			  
            </div><!--/.col (right) -->
		  
		  
		  
		  
		  
			</div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  @endsection
  
  @stop