@extends('admin/layouts/main')

@section('content')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		
          <h1>
		  
           {{$user->username}} Profile
           
          </h1>
         
		  
        </section>
		
        <!-- Main content -->
        <section class="content">
		
          <!-- Your Page Content Here -->
		  <div class="row">
            <div class="col-xs-12">
			
			
			
			      <div class="box">
				  
				  <div class="box-header">
				
				
				<?php $save_chack = Session::get('save');?>
				@if(!empty($save_chack))
				<div class="container-fluid">
				<div class="alert alert-dismissable alert-success">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
				<h4>{{Session::get('save') }}</h4>
					{{Session::forget('save') }}
				</div>
				</div>
				@endif
				
				
				<?php $error_check = Session::get('error');?>
				@if(!empty($error_check))
				<div class="container-fluid">
				<div class="alert alert-dismissable alert-danger">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
					<h4>{{Session::get('error') }}</h4>
					{{Session::forget('error') }}
				</div>
				</div>
				@endif
				
				
				
                  <a href="{{url('users/create')}}" class="btn btn-primary pull-right">Add User</a>
                </div><!-- /.box-header -->
               
			    <div class="box-body">
				
			<table style="width:100%" class="table table-bordered table-heading" >
			<tr>
			<th  style="width:30%" > username:</th>
			<td>{{$user->username}}</td>
			</tr>
			
			<tr>
			<th  style="width:30%" > fullname:</th>
			<td>{{$user->fullname}}</td>
			</tr>
			
			<tr>
			<th  style="width:30%" > email:</th>
			<td>{{$user->email}}</td>
			</tr>
			
			</table>
				
              </div><!-- /.box body -->
              </div><!-- /.box -->
			
			
			
			
			
			
			
			
			
			
			
			 </div><!-- /.col -->
          </div><!-- /.row -->
		  
		  
		  
		  

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  @endsection
  
  @stop