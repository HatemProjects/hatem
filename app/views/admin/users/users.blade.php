@extends('admin/layouts/main')

@section('content')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
		
          <h1>
		  
            Users
           
          </h1>
         
		  
        </section>
		
        <!-- Main content -->
        <section class="content">
		
          <!-- Your Page Content Here -->
		  <div class="row">
            <div class="col-xs-12">
			
			
			
			      <div class="box">
                <div class="box-header">
				
				
				<?php $save_chack = Session::get('save');?>
				@if(!empty($save_chack))
				<div class="container-fluid">
				<div class="alert alert-dismissable alert-success">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
				<h4>{{Session::get('save') }}</h4>
					{{Session::forget('save') }}
				</div>
				</div>
				@endif
				
				
				<?php $error_check = Session::get('error');?>
				@if(!empty($error_check))
				<div class="container-fluid">
				<div class="alert alert-dismissable alert-danger">
				<button class="close" aria-hidder="true" data-dismiss="alert">&times;</button>
					<h4>{{Session::get('error') }}</h4>
					{{Session::forget('error') }}
				</div>
				</div>
				@endif
				
				
				
                  <a href="{{url('users/create')}}" class="btn btn-primary pull-right">Add User</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Full Name</th>
                        <th>User name</th>
                        <th>Email</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
					@foreach ($users as $user)
                      <tr>
                        <td>{{$user->fullname}}</td>
                        <td>{{$user->username}}</td>
                        <td>{{$user->email}}</td>
						
						
						
                       <td><a href="{{url('users/'.$user->id.'/edit')}}" class="btn btn-primary" >Edit</a></td>
                        
						<td>
						
						{{Form::open(array('method'=>'delete','url'=>'users/'.$user->id))}}
						
						<button type="submit" class="btn btn-danger">Delete</button>
						
						
						{{Form::close()}}
						
						</td>
						
						
						
						
						
                      </tr>
                     @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Full Name</th>
                        <th>User name</th>
                        <th>Email</th>
                        <th>Edit</th>
                        <th>Delete</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
			
			
			
			
			
			
			
			
			
			
			
			 </div><!-- /.col -->
          </div><!-- /.row -->
		  
		  
		  
		  

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

  @endsection
  
  @stop