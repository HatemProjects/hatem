-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2016 at 01:19 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hatem`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` int(2) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_type`, `username`, `fullname`, `password`, `email`, `remember_token`, `created_at`, `updated_at`) VALUES
(16, 1, 'admin', 'admin', '$2y$10$snQhcFfaB6uTewzNbsWNOu0LoWUEwDjxK5CIiXND7u3vBgMdk4yPe', 'admin@admin.com', 'WyLJkfGtjdiIhfaAeOVDt7qnU0RqyYOQm82ilBpBX8dPPRw9QjZCmsKSAlFc', '2015-12-21 11:36:48', '2016-04-02 20:29:06'),
(35, 2, 'test2', 'test2', '$2y$10$U7vSeaLHJ8BntR.8E3DKBeUpNhcqvOVd7lssgnkaJzuMD3I3sNgp6', 'test@test.com', '', '2016-04-02 18:11:57', '2016-04-02 20:27:37'),
(36, 2, 'ahmed', 'ahmed', '$2y$10$MtCPNNgtv9R/a/lanpuxTu8kC2IbHs3Ec.XY9USN6NagOJNx2cyn2', 'ahmed@ahmed.com', 'xIdWirt0lrOfaqxlcoEDgSLIB0tXh1QqlpHvttKCzVxmFoHz5VwEJWvDSpBe', '2016-04-02 19:49:59', '2016-04-02 20:27:12'),
(37, 2, 'ahmed2', 'ahmed', '$2y$10$glLplnz8W9pscLUXr7o4d.lJ.xQNembCFF7EfQpxjvOCAvuL.qoki', 'ahmed@ahmed.com', 'MGZJoh8yCUarvsG7vm7Kb2IO9s4fjylMr22zuhSBFMY7mPxMO6qHY2Q63Jwb', '2016-04-02 20:29:58', '2016-04-02 20:54:34'),
(38, 2, 'ahmed33', 'ahmed333', '$2y$10$NsSBeCm2PDQunPSwRdxPeebHmnvisDX3yBfVCKsLBCPcnhdjQgIx6', 'ahmed@ahmed.com', '', '2016-04-02 20:58:48', '2016-04-02 20:58:48');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
